# Node.js Express REST API

Simple Express API Playground

### Dependencies

* Express, Sequelize.

### Installing

* Coming soon...

### Executing program

* Coming soon...
```
<?php echo 'Hello World!' ?>
```

## Help

Coming soon...
```
<?php echo 'Hello World!' ?>
```

## Authors
Hayrettin Gokturk
[@gokturkh](https://bitbucket.org/gokturkh)

## Version History
* 0.1
    * Initial Release

## License

This project is licensed under the [MIT] License - see the LICENSE.md file for details
