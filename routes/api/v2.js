var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.json({ api: "v2" });
});

/* router.get('/:userId/books/:bookId', function (req, res) {
  res.send(req.params)
}) */

module.exports = router;
