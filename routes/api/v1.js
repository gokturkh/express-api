var express = require('express');
var router = express.Router();
const companyController = require('../../controllers').company;

/* GET users listing. */
router.get('/', function (req, res, next) {
    res.json({ api: "v1" });
});

router.get('/companies', companyController.index);
router.post('/company/add', companyController.create);

/* router.get('/:userId/books/:bookId', function (req, res) {
  res.send(req.params)
}) */

module.exports = router;
